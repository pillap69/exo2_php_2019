<!DOCTYPE html>
<!--
git clone https://pillap69@bitbucket.org/pillap69/exo1_2019_php.git
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        require_once 'mesClasses/Cvisiteurs.php';

        
        try {
            
            $ovisiteurs = new Cvisiteurs(); //instanciation de la classe de contrôle 
            $ovisiteur = $ovisiteurs->getVisiteurById('a17'); // Récupétation du visiteur à partir de s
            if($ovisiteur == null)
            {
                throw  new Exception("Objet non instancié car l'ID n'est pas valable !");
            
            }
            echo 'le nom du visiteur est : ' . $ovisiteur->nom;
        
            echo '<br>' ;//<!-- Un passage à la ligne -->

        ?>
            Le nom du visiteur est :  <?=$ovisiteur->nom?>
        <?php
            echo '<br>';
            //<!--Affichage dans une zone de texte HTML à l'aide d'un echo php-->
            echo 'Le nom du visiteur est : <input type="text" value="'.$ovisiteur->nom.'" name="username">';
           
        ?>
        
        <?php
        } catch (Exception $e) {
    
                $msg = 'ERREUR : ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
                die($msg);
                              
        }
        ?>
        
        
    </body>
</html>
