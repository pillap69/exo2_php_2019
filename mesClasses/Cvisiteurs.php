<?php

require_once 'Cdao.php'; //classe outils
/* ************ Classe métier Cvisiteur et Classe de contrôle Cvisiteurs **************** */

class Cvisiteur
{

    public $id;
    public $login;
    public $nom;
    public $prenom;


    function __construct($sid,$slogin,$snom,$sprenom) //s pour send param envoyé
    {
        

        $this->id = $sid;
        $this->login = $slogin;
        $this->nom = $snom;
        $this->prenom = $sprenom;
    }
}


class Cvisiteurs 
{
 
    private $ocollVisiteurById;
    

    public function __construct()
    {
       
                  try {

                            
                             $query = 'SELEC id,login,nom,prenom from visiteur';
                             $odao = new Cdao();
                             $lesVisiteurs = $odao->getTabDataFromSql($query);                       
                             $this->ocollVisiteur = array();
                             
                             
                             foreach ($lesVisiteurs as $unVisiteur) {
                                 
                                $ovisiteur = new Cvisiteur($unVisiteur['id'],$unVisiteur['login'],$unVisiteur['nom'],$unVisiteur['prenom']);
                                $this->ocollVisiteur[$unVisiteur['id']] = $ovisiteur;
                                
                            } 
                            $this->ocollVisiteurById = $this->ocollVisiteur;
                            
                        /* fin 1ER cas */

                      }
                  catch(PDOException $e) {
                         $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
                         die($msg);
                        }
   

    }



    function getVisiteurById($sid)
    {
            if(array_key_exists ( $sid , $this->ocollVisiteurById))
            {
                $ovisiteur = $this->ocollVisiteurById[$sid];
                return $ovisiteur;
            }
                 
        
    }
    
    

}


